#include <Arduino.h>
#include <HTTPClient.h>
#include <WiFi.h>

const char *ssid = "Livebox-7DFC";
const char *password = "xAY6oVdkkWqwfjfxEe";

unsigned long lastTime = 0;
unsigned long timerDelay = 10000;

// Your Domain name with URL path or IP address with path
String serverName = "http://example.org"; //"https://api4.my-ip.io/ip";


/* The Certificat of the WEB site since it is in HTTPS */
const char* root_ca = \
"-----BEGIN CERTIFICATE-----\n"\
"MIIDgDCCAmigAwIBAgIQGAytRmT85fAinUTJFQhQojANBgkqhkiG9w0BAQsFADBI\n" \
"MRswGQYDVQQDDBJFU0VUIFNTTCBGaWx0ZXIgQ0ExHDAaBgNVBAoME0VTRVQsIHNw\n" \
"b2wuIHMgci4gby4xCzAJBgNVBAYTAlNLMB4XDTIyMDUzMDA3MTc0NloXDTMyMDUy\n" \
"NzA3MTc0NlowSDEbMBkGA1UEAwwSRVNFVCBTU0wgRmlsdGVyIENBMRwwGgYDVQQK\n" \
"DBNFU0VULCBzcG9sLiBzIHIuIG8uMQswCQYDVQQGEwJTSzCCASIwDQYJKoZIhvcN\n" \
"AQEBBQADggEPADCCAQoCggEBALuxkkK+3eQ8FNZf04QYSr/etgHuVpnpyFn7PFzq\n" \
"HUDCa88y0DbfxeHeOtyna4UmkN6FNEhPmJtYsdPbFr6wABM5XBxF3WfCg17fAHZR\n" \
"pyhLnxhrYDQoENCFVdsvTLDB0GrOwFinsKQEku3HES+twf34vIQtaoDt4kVcddCp\n" \
"aJ0dcANwxIxa9SAaGR9SA8J9IycOSU8n2uYygmsU4UGOgiOp/5ITlLr8MI0U1F7J\n" \
"AKQ7h6TrsyfBD5HEXDjbVv6CD//BQjlgTgoSSRf4+kCC/Z9aC2uILedj+P9SdMhj\n" \
"mJxpwjOvzUeVta2Rdt65fCfxV8TTFY6tRrVs87ljlz29sxsCAwEAAaNmMGQwDgYD\n" \
"VR0PAQH/BAQDAgIEMBIGA1UdEwEB/wQIMAYBAf8CAQAwHQYDVR0OBBYEFIlzsKT0\n" \
"VpJZFmXqkUtXYLcr7JgyMB8GA1UdIwQYMBaAFIlzsKT0VpJZFmXqkUtXYLcr7Jgy\n" \
"MA0GCSqGSIb3DQEBCwUAA4IBAQBLOSwXwE0z/+mQ1h6oXUBDhHHqZnfprnInPQXp\n" \
"pDZ+AV6/NJ0X4eGd2eMeYjRZ183SCW/51fy6mfFIuxcGGeb/nL/7lrhKvH78O42z\n" \
"oSaB4qAQHJ2uAQmnB3EyY10VtxkLdFl8O3D7QaFxkL9g9rm+k8RwdrnuouguCp9v\n" \
"/khii7T9gzLebDI9utraxqyF2BRmndlOnqxWsEKOArS3yNb/x4uDekrY9m4FyUod\n" \
"ASiN3beM0dds7e63sTD4JMtilUvf58h6oB6aRRx++19Rss9/2Tzs2up5V9nZY8xO\n" \
"GeV3yxtYQf0uzsZGt+WU7wgxZvANWcM3FRQm92N3JgZkrm2B\n" \
"-----END CERTIFICATE-----\n";


void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);
  /*   //For debug purpose
  while (!Serial)
  {
  } */
  Serial.println("\n");

  WiFi.begin(ssid, password);
  Serial.print("Connecting to Florelimou networks");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Timer set to 5 seconds (timerDelay variable), it will take 5 seconds before publishing the first reading.");
}

void loop()
{
  // put your main code here, to run repeatedly:
  if ((millis() - lastTime) > timerDelay)
  {
    // Check WiFi connection status
    if (WiFi.status() == WL_CONNECTED)
    {
      HTTPClient http;

      // String serverPath = serverName + "?temperature=24.37";

      // Your Domain name with URL path or IP address with path
      http.begin(serverName, root_ca);

      // Send HTTP GET request
      int httpResponseCode = http.GET();

      if (httpResponseCode > 0)
      {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        String payload = http.getString();
        Serial.println(payload);
      }
      else
      {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
      }
      // Free resources
      http.end();
    }
    else
    {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}